		
		var selectedtool = null;
		var dtools = {
			pen: new Pen(),
			vector: null
		}
		console.log(dtools);

		var hotkeys = {

		}

		var utils = {
			showmsg: function(msg){
				$('#xtoast').html(msg);
				$('#xtoast').fadeIn();
				$('#xtoast').css('left',$(window).width()/2  - $('#xtoast').width())
				setTimeout(function(){
					$("#xtoast").fadeOut()
				},3000);
			}
		}

		//Pen Class

		function Pen() {
			this.ppts = [];

		}

		Pen.prototype.preDraw = function(e) {
			this.ppts.push(ecanvas.adjustOffset(e.pageX, e.pageY));
		}

		Pen.prototype.draw = function(brushstyle) {
			var ctx = ecanvas.buffer_context;
			var ppts = this.ppts;

			if (ppts.length < 3) {
				var b = ppts[0];
				ctx.beginPath();
				ctx.arc(b.x, b.y, ctx.lineWidth/ 2, 0, Math.PI * 2, !0);
				ctx.fill();
				ctx.closePath();
				
				return;
			}
			
			//Clear buffer before draw
			ecanvas.clearBuffer();
			
			if(brushstyle != undefined){
    			 ctx.lineWidth = brushstyle.pointsize;
    			 ctx.lineJoin = 'round';
	 			 ctx.lineCap = 'round';
  			}

  			ctx.strokeStyle = ecanvas.foreground_color;

			ctx.beginPath();
			ctx.moveTo(ppts[0].x, ppts[0].y);

			for(var i = 1; i < ppts.length-2; i++){
				var c = (ppts[i].x + ppts[i + 1].x) / 2;
				var d = (ppts[i].y + ppts[i + 1].y) / 2;
				
				ctx.quadraticCurveTo(ppts[i].x, ppts[i].y, c, d);
			}

			ctx.quadraticCurveTo(
				ppts[i].x,
				ppts[i].y,
				ppts[i + 1].x,
				ppts[i + 1].y
			);
			ctx.stroke();

		}

		Pen.prototype.mousedown = function(e) {

				console.log("Clicked on canvas at " + e.pageX + "," + e.pageY)
				ecanvas.registerNewDrawing( e.pageX, e.pageY);
				
				this.preDraw(e);
				this.draw();
		}

		Pen.prototype.mousemove = function(e) {
				if(!ecanvas.state_drawingstarted){
					return;
				}
				
				var mybrush = new BrushStyle($('.propertycontainer .linewidth').val()); //TODO: Creates a property namespace to manage properties of 		current 	tools
				
				this.preDraw(e);
				this.draw(mybrush);
				
		}

		Pen.prototype.mouseup = function(e) {
			console.log("Stopped Drawing Motion");
			ecanvas.state_drawingstarted = false;
			ecanvas.pasteBuffer();
			this.ppts = [];
		}
