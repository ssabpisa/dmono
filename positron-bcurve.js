/*
	positron bezier curve control
	Classes for <BezierCurve>

*/
function BezierNode(base,cPrev,cNext) {
	/*
		base - node coordinate
		cPrev - control vector for previous curve
		cNext - control vector for next curve
		cLock - true for adjustment to cNext will also move cPrev in opposite direction and vice versa
		
	*/
	if(cPrev == undefined) cPrev = null;
	if(cNext == undefined) cNext = null;
	this.base = base;
	this.cPrev = cPrev;
	this.cNext = cNext;
	this.cLock = true;
	
}
BezierNode.prototype.translate = function(deltaX, deltaY) {
	this.base.x += deltaX;
	this.base.y += deltaY;
	
	if(cPrev != null){
		cPrev.x += deltaX;
		cPrev.y += deltaY;
	}
	if(cNext != null){
		cNext.x += deltaX;
		cNext.y += deltaY;
	}
}

BezierNode.prototype.translateCPrevious = function(deltaX, deltaY) {
	
}

function BezierCurve(startNode) {
	
}

function addNode(bezierNodes, vector1) {

}
