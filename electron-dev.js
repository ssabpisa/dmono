/*
	electron drawing engine HTML5 
	Classes for <ElectronCanvas>

	For unrelated classes/members create it elsewhere
*/

function Vector(x,y,z){
	if(z == undefined) z = 0;

	this.x  = x;
	this.y = y;
	this.z = z;
}

function BrushStyle(pointsize){
	this.penface = '';
	this.pointsize = pointsize;
}

function ElectronCanvas(width,height,canvas){
	this.width = width;
	this.height = height;
	this.context = canvas.getContext("2d");

	//Temporary Canvas
	this.buffer_canvas = document.createElement('canvas');
	this.buffer_context = this.buffer_canvas.getContext('2d');
	this.buffer_canvas.id = 'buffer_canvas';
	this.buffer_canvas.width = canvas.width;
	this.buffer_canvas.height = canvas.height;
	this.buffer_canvas.className = 'canvas';
	this.buffer_canvas.style.backgroundColor='transparent';
	

	//Attach it to existing canvas
	$(canvas).after(this.buffer_canvas);

	this.layers = []; //List of layers

	this.last_mouse = new Vector(-1,-1);
	this.offset_left = 0;
	this.offset_top = 0;

	this.ppts = [];

	this.foreground_color = '#000000';
	this.background_color = '#FFFFFF';

	this.state_drawingstarted = false;

	console.log("Canvas created");
}

ElectronCanvas.prototype.reloadLayers = function(){
	//Reload layers into canvas
	//and onto layer list
	$('.layerblock').remove();
	
	if(this.layers.length == 0){
		//TODO: Show empty message in toolbox
		return;
	}
	
	$(this.layers).each(function(index){
		var selectionCls = "";
		if(index == 0) selectionCls = "layerblock_active";
		$(".layerlist").append('<div data-layerID="' + this.id + '" class="layerblock ' + selectionCls + '"><strong>'  + this.label + '</strong></div>');
	})

	//Initialize layer manager
	$("#layerbox .layerblock").click(function(){
		$('.layerblock').removeClass("layerblock_active");
		$(this).addClass('layerblock_active');

		console.log("Layer selected: " + $(this).attr("data-layerID"));
		// ecanvas.changeLayer()
	})


}

ElectronCanvas.prototype.focusOnLayer = function(LayerObject){
	//When user click on layer from the layer list, 
	//Focus on it on the canvas
}

ElectronCanvas.prototype.createNewLayer = function(){
	ecanvas.layers.push(new Layer(ecanvas,ecanvas.layers.length + 1,"Layer " + Number(ecanvas.layers.length + 1)));
	ecanvas.reloadLayers();
}

ElectronCanvas.prototype.clearBuffer = function() {
	this.buffer_context.clearRect(0,0,this.buffer_canvas.width, this.buffer_canvas.height);
}

ElectronCanvas.prototype.pasteBuffer = function() {
	this.context.drawImage(this.buffer_canvas, 0, 0);
	console.log(this.context);
}

ElectronCanvas.prototype.pasteAndClearBuffer = function() {
	this.pasteBuffer();
	this.clearBuffer();
}


ElectronCanvas.prototype.registerNewDrawing = function(x,y){
	this.state_drawingstarted = true;
	this.last_mouse.x = x;
	this.last_mouse.y = y;
}


ElectronCanvas.prototype.loadFromImage = function(url) {
	this.context.fillRect(5,5,100,55);
    console.log("Canvas loaded with image from url " + url);
};

ElectronCanvas.prototype.drawBCurve = function(bezierNodes)
{		
		var _this = this;
		bezierNodes = new Array(
								new BezierNode( new Vector(10,10),
												null,
												null),
								new BezierNode( new Vector(500,500),
												null,
												null)
								);
		var debug = true;
		
		_this.context.beginPath();
		
		$.each(bezierNodes, function(index, node) {
			if(index == 0){ //Start node
				console.log("Drawing BNode " + index + " with Start Node");
				_this.context.moveTo(node.base.x, node.base.y);
			}	
			else {
				var previousnode = bezierNodes[index-1];
				var controlPrev = (previousnode.cNext == null ) ? node.base : previousnode.cNext;
				var controlNext = (node.cPrev == null) ? node.base : node.cPrev;
				
				_this.context.bezierCurveTo(controlPrev.x,
											controlPrev.y,
											controlNext.x,
											controlNext.y,
											node.base.x,
											node.base.y);
			}
		});
		
		_this.context.stroke();
		
		this.drawBCurveControl(bezierNodes);
}

ElectronCanvas.prototype.drawBCurveControl = function(bezierNodes){
		_this.context.beginPath();
		$.each(bezierNodes, function(index, node) {
				var length=5;
				_this.context.fillRect(node.base.x-length/2,node.base.y-length/2,length,length);
		});
		_this.context.stroke();
}

ElectronCanvas.prototype.drawLine = function(A,B,Style) {
	//Draw line from A to B 
    this.context.beginPath();
    this.context.moveTo(A.x, A.y);
    this.context.lineTo(B.x, B.y);
	this.context.lineWidth = 1;
	this.context.strokeStyle = '#ff0000'
    this.context.stroke();

};

/* MOVE TO Pen class
ElectronCanvas.prototype.freedraw = function(position, brush){
	var ctx = this.context;
	
	//Quadratic Calculation (TODO: confirm its performance)
	var quad_controlx = ( (this.last_mouse.x-this.offset_left) + (position.x-this.offset_left) )/2;
	var quad_controly = ( (this.last_mouse.y-this.offset_top) + (position.y-this.offset_top) )/2;

	ctx.beginPath();

    ctx.moveTo(this.last_mouse.x  - this.offset_left, this.last_mouse.y - this.offset_top); //then continue drawing from previous dot

    ctx.quadraticCurveTo(quad_controlx, quad_controly, position.x - this.offset_left, position.y - this.offset_top); //and up to this dot
    ctx.closePath(); //connect the two
    ctx.strokeStyle = this.foreground_color;

    if(brush != undefined){
    	 ctx.lineWidth = brush.pointsize;
    	 ctx.lineJoin = 'round';
	 	 ctx.lineCap = 'round';
    }

    ctx.stroke(); 

    this.last_mouse.x = position.x; //remember the last position
	this.last_mouse.y = position.y;
} */


ElectronCanvas.prototype.clear = function() {
    console.log("Canvas cleared");
};

ElectronCanvas.prototype.changeForeground = function(rbg){
	this.foreground_color = rbg;
	console.log("Color foreground " + rbg);

}

ElectronCanvas.prototype.adjustOffset = function(x,y) {
	return new Vector(x-this.offset_left, y-this.offset_top);
}

//Keep track of mousemove
ElectronCanvas.prototype.mousemove = function(e) {
	if(this.last_mouse != undefined){
		this.last_mouse.x = e.pageX;
		this.last_mouse.y = e.pageY;
	}
}



//=========== Broadcaster ==============

//Broadcast List
var ev_broadcast_list = [];

function ev_broadcast(ev) {

	//TODO: Add base event func (for hotkey etc.)

	$.each(ev_broadcast_list, function(index, get) {
		var broadcastee = get();		
		if(broadcastee != null) {

			//Relay to correct event func
			var func = broadcastee[ev.type];
			if(func){
				broadcastee[ev.type](ev);
			}
		}

	});
}
